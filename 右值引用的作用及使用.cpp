#include <iostream>
using namespace std;
#include <string>
#include <functional>

//C++11新加了一个右值引用，标记为&&  
//左值引用和右值引用的区别：
//左值是指存储在内存中、有明确存储地址（可取地址）的数据
//右值是可以提供数据值的数据（不可以取地址）
//区分左值于右值的便捷方法是：可以对表达式取地址（&）就是左值，否则为右值
//无论是声明左值引用还是右值引用都必须立即进行初始化。
//但无论是左值引用还是右值引用都是为了提高系统执行效率

class Test
{
public:
	Test() :m_num(new int(100))
	{
		cout << "construct:my name is jerry" << endl;
		printf("m_num 地址：%p\n", m_num);
	}
	Test(const Test& a) :m_num(new int(*a.m_num))//提供一个拷贝构造函数 避免浅拷贝带来的问题
	{
		cout << "copy construct:my name is tom" << endl;
	}
	//移动构造函数->复用其他对象中的资源（堆内存） m_num 注意：移动构造需要做浅拷贝
	//Test(Test&& a) :m_num(a.m_num)
	//{
	//	a.m_num = nullptr;
	//	cout << "move construct..." << endl;
	//	//相当于做了一次堆内存的转移
	//}
	~Test()
	{
		cout << "destruct Test class ..." << endl;
		delete m_num;
	}

	int* m_num;
};

Test getopj()
{
	Test t;
	return t;
}

Test getopj1()
{
	return Test();
}

//返回右值引用
Test&& getopj2()
{
	return Test();
}

int main()
{
	//简单介绍一下
	//左值
	int num = 9;//ok
	//左值引用
	int& a = num;//ok
	//右值
	//右值引用
	int&& b = 8;//ok
	//常量右值引用
	const int&& d = 6;//ok
	//const int&& d = b;//error
	//int&& f = d;//error
	//常量左值引用
	const int& c = num;//ok
	const int& f = b;//ok
	const int& h = d;//ok
	
	//给某一个类指定了移动构造函数 调用移动构造函数的方法有这两种 ，但右值必须是临时对象
	//如果没有移动构造函数，就会调用拷贝构造函数
	Test t = getopj(); //可以发现getopj函数里面的t对象并没有使用，只是通过临时对象给外边的对象赋值操作   这样会耗费系统资源，利用右值引用优化这个问题
	//必须要临时对象才会优先调用移动构造函数
	cout << endl;
	Test&& t1 = getopj();//这样也可以
	printf("m_num 地址：%p\n", t1.m_num);
	cout << endl;

	//如果没有移动构造函数，使用右值引用初始化要求更高一些
	//要求右侧是一个临时的不能取地址的对象
	Test&& t2 = getopj1();
	printf("m_num 地址：%p\n", t1.m_num);

	return 0;
}
