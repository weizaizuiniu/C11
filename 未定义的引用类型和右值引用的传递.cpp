#include <iostream>
using namespace std;

class Test
{
public:
	Test() :m_num(new int(100))
	{
		cout << "construct:my name is jerry" << endl;
		printf("m_num 地址：%p\n", m_num);
	}
	Test(const Test& a) :m_num(new int(*a.m_num))//提供一个拷贝构造函数 避免浅拷贝带来的问题
	{
		cout << "copy construct:my name is tom" << endl;
	}
	//移动构造函数->复用其他对象中的资源（堆内存） m_num 注意：移动构造需要做浅拷贝
	Test(Test&& a) :m_num(a.m_num)
	{
		a.m_num = nullptr;
		cout << "move construct..." << endl;
		//相当于做了一次堆内存的转移
	}
	~Test()
	{
		cout << "destruct Test class ..." << endl;
		delete m_num;
	}

	int* m_num;
};

Test getopj()
{
	Test t;
	return t;
}

void printvalue(int& i)
{
	cout << "l-value:" << i << endl;
}

void printvalue(int&& i)
{
	cout << "r-value:" << i << endl;
}

void forward(int&& k)
{
	printvalue(k);
}
int main()
{
	//&&的特性：在C++中不是所有的&&都代表一个右值引用，具体的场景体现在模板自动类型推导中
	//通过右值推导T&&或者auto&&得到的是一个右值引用类型
	//通过非右值（右值引用、左值、左值引用、常量右值引用、常量左值引用）推导T&&或者auto&&得到的是一个左值引用类型

	//Test t = getopj();

	int&& a1 = 2;//右值引用
	auto&& b1 = a1;//左值引用
	auto&& b2 = 1;//右值引用

	int a2 = 5;
	int& a3 = a2;//左值引用
	auto&& c1= a3;//左值引用
	auto&& c2 = a2;//左值引用

	const int& s1 = 100;//常量左值引用
	const int&& s2 = 100;//常量右值引用
	auto&& d1 = s1;//常量左值引用
	auto&& d2 = s2;//常量左值引用

	const auto&& x = 5;//常量右值引用

	//右值引用的转递
	int i = 100;
	printvalue(i);    //输出结果：l - value:100   调用的是左值引用
	printvalue(1000); //输出结果：r - value:1000  调用的是右值引用
	forward(250);     //输出结果：l - value:250   调用的是右值引用的forward函数，而forward函数的右值引用有了名字调用的是左值引用

	//最后总结一下关于 && 的使用
	//1、左值和右值是独立于它们的类型的，右值引用类型可能是左值也可能是右值
	//2、编译器会将已命名的右值引用视为左值，将未命名的右值引用视为右值
	//3、auto&& 或者函数参数类型自动推导的 T&& 是一个未定的引用类型，它可能是左值引用也可能是右值引用类型，这取决于初始化的值类型（上面有例子）
	//4、通过右值推导 T&& 或者 auto&& 得到的是一个右值引用类型，其余都是左值引用类型

	return 0;
}
