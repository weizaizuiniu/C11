#include <iostream>
using namespace std;
#include <string>
#include <functional>
#include <list>

//在C++11中右值引用，并且不能直接使用左值来初始化一个右值引用，如果想要使用一个左值初始化一个右值引用需要借助 std::move() 函数。
//使用 std::move 方法可以将左值转换成右值，使用这个函数并不能移动任何东西，而是和移动构造函数一样具有移动语义，将对象的状态或者所有权从一个对象转移到另一个对象，只是转移没有内存拷贝
//本质就是把“一个人”的一些资源的所有权转移给“另一个人”


class Test
{
public:
	Test() :m_num(new int(100))
	{
		cout << "construct:my name is jerry" << endl;
		printf("m_num 地址：%p\n", m_num);
	}
	Test(const Test& a) :m_num(new int(*a.m_num))//提供一个拷贝构造函数 避免浅拷贝带来的问题
	{
		cout << "copy construct:my name is tom" << endl;
	}
	//移动构造函数->复用其他对象中的资源（堆内存） m_num 注意：移动构造需要做浅拷贝
	Test(Test&& a) :m_num(a.m_num)
	{
		a.m_num = nullptr;
		cout << "move construct..." << endl;
		//相当于做了一次堆内存的转移
	}
	~Test()
	{
		cout << "destruct Test class ..." << endl;
		delete m_num;
	}

	int* m_num;
};

Test getopj()
{
	Test t;
	return t;
}

Test getopj1()
{
	return Test();
}

//返回右值引用
Test&& getopj2()
{
	return Test();
}

int main()
{
	//简单介绍一下
	//左值
	int num = 9;//ok
	//左值引用
	int& a = num;//ok
	//右值
	//右值引用
	int&& b = 8;//ok
	//常量右值引用
	const int&& d = 6;//ok
	//const int&& d = b;//error
	//int&& f = d;//error
	//常量左值引用
	const int& c = num;//ok
	const int& f = b;//ok
	const int& h = d;//ok

	//给某一个类指定了移动构造函数 调用移动构造函数的方法有这两种 ，但右值必须是临时对象
	//如果没有移动构造函数，就会调用拷贝构造函数
	Test t = getopj(); //可以发现getopj函数里面的t对象并没有使用，只是通过临时对象给外边的对象赋值操作   这样会耗费系统资源，利用右值引用优化这个问题
	//必须要临时对象才会优先调用移动构造函数
	cout << endl;
	Test&& t1 = getopj();//这样也可以
	printf("m_num 地址：%p\n", t1.m_num);

	//如果没有移动构造函数，使用右值引用初始化要求更高一些
	//要求右侧是一个临时的不能取地址的对象
	cout << endl;
	Test&& t2 = getopj1();
	printf("m_num 地址：%p\n", t1.m_num);

	//Test&& t3 = t2;//error 因为t2不是匿名的临时对象，编译器会把t2看成左值引用，左值引用不能初始化一个右值引用  可以利用 move 函数转移
	Test&& t3 = move(t2);//ok
	Test&& t4 = move(t);//ok

	list<string> ls1{ "hello","world","tom","select","from","join","where","order" };
	list<string> ls2 = ls1;//对象ls1对进行一个拷贝数据到ls2对象中，如果后续ls1不需要用的就可以使用 move 来进行数据转移，就不需要拷贝了，注意：ls1必须确定不需要用了才可以转移
	list<string> ls3 = move(ls1);

	//测试代码
	for (auto it = ls1.begin(); it != ls1.end(); it++)
	{
		cout << "ls1:" << *it << "  " << " ";//这时的ls1容器内没有数据
	}
	cout << endl;
	for (auto it = ls2.begin(); it != ls2.end(); it++)
	{
		cout << "ls2:" << *it << "  " << " ";//输出结果：ls2:hello   ls2:world   ls2:tom   ls2:select   ls2:from   ls2:join   ls2:where   ls2:order
	}
	cout << endl;
	return 0;
}