#include <iostream>
using namespace std;
#include <string>


class base
{
public:
	base(int i) :m_i(i) {}
	base(int i, string j) :m_i(i), m_j(j) {}
	base(int i, string j, double k) :m_i(i), m_j(j), m_k(k) {}

	void func(int i)
	{
		cout << "base i = " << i << endl;
	}
	void func(int i, string str)
	{
		cout << "base i = " << i << ",str = " << str << endl;
	}

	int m_i;
	string m_j;
	double m_k;
};

class child :public base
{
public:
	void func()
	{
		cout << "child func" << endl;
	}

	////******未使用继承构造函数*****************
	////委托构造函数   子类调用父类的构造函数
	//child(int i) :base(i) {}
	//child(int i, string j) :base(i, j) {}
	//child(int i, string j, double k) :base(i, j, k) {}
	
	//******使用继承构造函数*****************
	using base::base;//子类可以使用父类的所有构造函数
	using base::func;//子类可以使用父类的所有func函数
};

int main()
{
	//测试代码
	child c(100, "I love you", 3.14);
	cout << c.m_i << "  " << c.m_j << "  " << c.m_k << endl;

	child f(200);
	c.func();

	//错误 因为在子类中会把父类的同名函数隐藏起来  解决办法：让子类“看到”  using base::func
	//c.func(19);
	//c.func(20, "tom");

	c.func(49);
	c.func(2, "dog");
	return 0;
}







