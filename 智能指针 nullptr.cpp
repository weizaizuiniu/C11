#include <iostream>
using namespace std;
#include <string>



void func(char* p)
{
	cout << "void func(char* p) 的调用" << endl;
}
void func(int p)
{
	cout << "void func(int* p) 的调用" << endl;
}
void test()
{
	int* ptr1 = NULL;
	double* ptr2 = NULL;
	char* ptr3 = NULL;
	void* ptr4 = NULL;

	// int 
	func(10);    //输出为void func(int* p) 的调用
	// char*
	func(NULL);    //输出为void func(int* p) 的调用   

	//结论：在C中 NULL表示（void*）0   C++中不允许一个void* 类型的变量隐式转换为其他类型的指针 如何需要转换为需要的类型指针需要显式类型转换
	int* ptr5 = (int*)ptr4;//显式类型转换

	//nullptr如果给一个int*类型指针初始化 nullptr会自动转换为int*  其他的同理
	int* ptr9 = nullptr;
	char* ptr6 = nullptr;
	void* ptr7 = nullptr;
	double* ptr8 = nullptr;

	// char*
	func(nullptr);    //输出为void func(char* p) 的调用   


	//总结：在C++中NULL和0是没有什么区别的 在给指针初始化时建议把NULL改为nullptr  
}

int main()
{
	//test();//指针空值类型  nullptr

	return 0;

}