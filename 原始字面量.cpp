#include <iostream>
using namespace std;
#include <string>

void test1()
{
	//原始字符串字面量定义方式：R“***（原始字符串）***”  注意 * 要前后对应  原始字符串遇到\是不会转义的   *可以理解为注释 编译阶段编译器会自动忽略
	string str1 = "D:\hello\world\test.c"; cout << str1 << endl;     //  helloworld    est.c

	string str2 = "D:\\hello\\world\\test.c"; cout << str2 << endl;  //  \hello\world\test.c

	string str3 = R"(D:\hello\world\test.c)"; cout << str3 << endl;  //  \hello\world\test.c

	//实用技巧：字符串中有\但不希望转义时使用 
}

int main()
{
	//test1();//测试原始字面量

	return 0;

}