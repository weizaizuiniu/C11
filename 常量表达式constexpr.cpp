#include <iostream>
using namespace std;
#include <string>


struct T
{
	int a;
	char b;

};
void test(const int num)  //num为只读变量 也称常变量
{
	//在C++11之前只有const关键字 ，从功能上来说这个关键字有两种语义：1、变量只读 2、修饰常量  
	const int count = 100;  //常量
	constexpr int a = 50;   //常量
	constexpr int b = 25;   //常量
	constexpr int c = a + b + 25;//常量表达式
	//a = 200;error
	//c = 188;error

	//测试
	//int arr[num];//error
	//int arr[count];//正确

	//常量表达式会在编译阶段就会得到结果   如果是非常量表达式的话会在运行后才会得到结果  因此常量表达式会大大提高程序的执行效率
	//在C++11前编译器是无法判断是常量表达式  C++11中可以用constexpr修饰常量表达式， constexpr不会像const一样产生歧义

	constexpr T s{ 10,20 };
	//s.a = 100;//error

}
int main()
{
	int num = 2321;
	test(num);//constexpr的使用
	return 0;

}