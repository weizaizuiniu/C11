#include <iostream>
using namespace std;

class person  //final
{
public:
	virtual void test()
	{
		cout << "person::virtual void test()的调用" << endl;
	}
};
class girl :public person //基类加上final关键字的话这里出错 
{
public:
	void test() final  //只能继承到这里，不允许当前类的子类重写该类的函数
	{
		cout << "girl::void test()的调用" << endl;
	}

	//void func() final{}//出错 不允许final修饰普通函数
};
class boy :public girl
{
public:
	//void test()  //出错了 不允许重写
	//{
	//	cout << "boy::void test()的调用" << endl;
	//}
};
int main()
{
	//final:
	//C++11添加了final关键字来限制某个类不能被继承，或者某个虚函数不能被重写
	//如果用final修饰函数，只能修饰虚函数，这样就阻止的子类重写父类中的这个函数了
	//使用final修饰过的类是不允许有派生类的
	
	return 0;
}
