#include <iostream>
using namespace std;
#include <string>
#include <list>
#include <map>

template<typename T, typename G>
auto add(T a, G b)->decltype(a + b)//返回值类型后置
{
	return a + b;
}
int main()
{
	//返回值类型后置一般用于泛型编程  在不知道模板是什么返回值的情况下可以用自动类型推导出来
	int x = 520;
	double y = 13.14;

	auto ret = add<int, double>(x, y);
	cout << ret << endl;
	return 0;
}
