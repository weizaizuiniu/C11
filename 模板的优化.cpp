#include <iostream>
using namespace std;


template <typename T = long, typename G = int>
void mytest(T t = 'A', G g = 'B')
{
	cout << t << "  " << g << endl;
}

int main()
{
	//C++11对于模板的优化

	//自动类型推导,根据传递的实参
	//mytest<char,char>
	mytest('a', 'b');

	//mytest<int,char>
	mytest<int>('a', 'b');

	//mytest<char,char>
	mytest<char>('a', 'b');

	//mytest<int,char>
	mytest<int,char>('a', 'b');

	//mytest<char,int>
	mytest<char,int>('a', 'b');

	//mytest<long,int>  这里没有显示指定类型也没有实参传递可以推导  所有找有没有默认的模板参数
	mytest();


	//C++11还优化的右尖括号 以前的 vector<vector>> 编译器会把最后的>>理解为右移操作符来解析的，所有会发生错误 之前的解决办法只有把>>用空格隔开vector<vector> >
	//在C++11以后就不需要这样子了     emmmmmmmmmmmm自我感觉说了跟没说一样 本人一直都是这么做的哈哈哈


	return 0;
}
