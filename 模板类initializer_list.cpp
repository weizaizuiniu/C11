#include <iostream>
using namespace std;
#include <vector>

void func(initializer_list<int> ls)
{
	auto it = ls.begin();
	for (; it != ls.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}

class test
{
public:
	test(initializer_list<string> ll)
	{
		for (auto it = ll.begin(); it != ll.end(); it++)
		{
			cout << *it << " ";
			m_name.push_back(*it);
		}
		cout << endl;
	}
private:
	vector<string> m_name;
};

int main()
{
	//在C++的STL容器中，可以进行任意长度的数据的初始化，使用初始化列表也只能进行固定参数的初始化，
	//如果想要做到和STL一样的任意长度初始化能力，可以使用initializer_list这个轻量级的模板类来实现

	//initializer_list特点：
	//它是一个轻量级的容器类型，内部定义了迭代器iterator等容器必须的概念，遍历时得到的迭代器是只读的
	//对于initializer_list<T>而言，它可以接受任意长度的初始化列表，但是要求元素必须是同类型的T
	//在initializer_list内部有三个成员接口：size()、begin()、end()。
	//initializer_list对象只能被整体初始化或赋值。

	func({ 1,2,3,4,5,6,7,8,9 });//利用初始化列表

	test t({ "jack","luck","tom" });//利用初始化列表
	test t1({ "hello","world" });//利用初始化列表

	//可以发现 不管初始化列表有多长，只需要一个initializer_list就可以接受全部
	return 0;
}
