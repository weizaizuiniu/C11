#include <iostream>
using namespace std;
#include <vector>
#include <map>
#include <string>

int main()
{
	//C++11基于范围的 for 循环  语法：
	//for (decharation : expression)
	//{
	//	//循环体
	//}
	//decharation表示遍历声明，在遍历过程中，当前被遍历到的元素会被存储到声明的变量中。
	//expression表示要遍历的对象，它可以是 表达式、容器、数组、初始化列表等...
	
	vector<int> v{ 1,2,3,4,5,6,7,8,9 };

	//普通的for循环
	for (auto it = v.begin(); it != v.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	//基于范围的for循环
	for (auto item : v)
	{
		cout << item << " ";//注意这里不需要解引用，因为item是一个int类型的变量而不是一个迭代器
	}//这样1会把v容器中的每个元素拷贝到item变量中，这样item修改不了v容器中的元素而且拷贝浪费空间资源
	cout << endl;

	//改良版
	for (auto &item : v)
	{
		cout << item++ << " ";//v容器中的元素全都完成加1
	}
	cout << endl;

	//测试
	/*for (auto &item : v)
	{
		cout << item << " ";// 2，3，4，5，6，7，8，9，10
	}
	cout << endl;*/

	//使用细节：1、在自动类型推导map容器时，auto推导出来的是pair这样的键值对，操作的方法和指针差不多：it->first  it->second
	//2、元素只读：对于set容器来说，内部元素是只读的，这是set容器特性决定的，因此在基于范围的for循环中auto & 会被视为const auto &
	//3、访问次数：基于范围的for循环在对一个容器遍历时是访问频率是一次还是多次呢？答案是一次！访问时会确定访问对象有多少个元素，这个循环的次数就决定下来了

	//总结：这两种for循环各有优劣 基于范围的for循环在动态访问时非常不安全，因为访问一次就决定的边界值，而在多线程中访问对象是随时会发现变化的
	//而普通的for循环的效率没有这么高，他每一个循环都需要判断其边界值，
	return 0;
}
