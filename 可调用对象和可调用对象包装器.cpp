#include <iostream>
using namespace std;
#include <string>
#include <functional>//function包装器的头文件


//普通函数
void print(int num, string name)
{
	cout << "id = " << num << ", name = " << name << endl;
}
using funcptr = void (*)(int, string);//这时的funcptr是一个可调用对象

//类
class Test
{
public:
	//重载小括号操作符
	void operator()(string msg)
	{
		cout << "仿函数：" << msg << endl;
	}
	
	//将类对象转换成一个函数指针
	operator funcptr()
	{
		return world; // return的需要于“funcptr”匹配 必须要return需要转换的函数指针类型  而且return hello;不行 因为hello是类对象的 而world是属于类的
	}

	void hello(int a, string s)
	{
		cout << "number = " << a<< ", name = " << s << endl;
	}
	static void world(int a, string s)
	{
		cout << "number = " << a << ", name = " << s << endl;
	}

	int m_id = 100;
	string m_name = "tom";
};

class A
{
public:
	//构造函数参数是一个包装器对象
	A(const function<void(int, string)>& f) :callback(f)
	{

	}
	void notify(int id, string name)
	{
		callback(id, name);//调用通过构造函数得到的函数指针
	}
private:
	function<void(int, string)> callback;
};
int main()
{
	//在C++中存在“可调用对象”这么一个概念，可调用对象有这几种定义：
	//1、是一个函数指针。
	//2、是一个具有operator（）成员函数的类对象（仿函数）
	//3、是一个可以转换为函数指针的类对象
	//4、是一个类成员函数指针或类成员指针

	Test t;
	t("叫我靓仔");//此时换调用仿函数输出“我是靓仔”

	Test tt;
	tt(19, "dog");//此时tt对象被转换成一个函数指针，operator funcptr()里面返回的world被调用了

	//类的函数指针
	funcptr f = Test::world;//ok  不可以指向非静态函数
	//funcptr f = Test::hello;//error
	using fptr = void (Test::*)(int, string);//定义一个指向类成员函数的指针类型
	fptr f1 = &Test::hello;

	//类的成员指针（变量）
	using ptr1 = int Test::*;
	ptr1 p = &Test::m_id;//需要取地址

	Test ttt;
	(ttt.*f1)(21, "叫我靓仔");//注意优先级
	ttt.*p = 520;
	cout << "m_id = " << ttt.m_id << endl;//520
    
	//***************************************************************************
	cout << "***************************************************************************" << endl;

	//这样使用起来太过于麻烦，需要在每个函数体内指定不同的可调用对象的类型
	//这时可以使用C++11可调用对象包装器直接指定一个函数，在函数参数指定为一种统一的类型，这样就可以接受这四类可调用对象了
	//可调用对象包装器function是一个类模板，需要包含一个functional头文件  
	//语法：function<返回值类型>(参数类型列表）diy_name = 可调用对象

	//用途：1、包装普通函数。
	function<void(int, string)> ff1 = print;//这里的ff1是不会被调用的，想要调用需要格外的再次指定调用
	//2、包装类的静态函数。
	function<void(int, string)> ff2 = Test::world;
	//3、包装仿函数。
	Test ta;
	function<void(string)> ff3 = ta;
	//4、包装转换为函数指针的对象
	Test tb;
	function<void(int,string)> ff4 = tb;

	//调用
	ff1(1, "abc");//输出：1 abc
	ff2(2, "ABC");//输出：2 ABC
	ff3("cat");//输出：仿函数：cat
	ff4(20, "靓仔");//输出：20 靓仔
	
	A a(print);
	a.notify(18, "美女");

	A ab(Test::world);
	ab.notify(100, "abcde");

	A ac(tb);
	ac.notify(3, "luffy");

	return 0;
}
