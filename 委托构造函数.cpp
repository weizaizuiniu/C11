#include <iostream>
using namespace std;
#include <map>

//测试类
class person
{
public:

	////*******未使用委托构造函数**************
	////发现有冗余代码
	//person() {};
	//person(int max)
	//{
	//	this->_max = max > 0 ? max : 100;
	//}
	//person(int max, int min)
	//{
	//	this->_max = max > 0 ? max : 100;
	//	this->_min = min > 0 && min < max ? min : 1;
	//}
	//person(int max, int min, int mid)
	//{
	//	this->_max = max > 0 ? max : 100;
	//	this->_min = min > 0 && min < max ? min : 1;
	//	this->_mid = mid<max&& mid>min ? mid : 50;
	//}

	//********使用委托构造函数****************
	person() {};
	person(int max)
	{
		this->_max = max > 0 ? max : 100;
	}
	person(int max, int min):person(max)//其中的person只能写在这里不可以写到函数体内部
	{
		this->_min = min > 0 && min < max ? min : 1;
	}
	person(int max, int min, int mid):person(max,min)
	{
		this->_mid = mid<max&& mid>min ? mid : 50;
	}
private:
	int _max;
	int _min;
	int _mid;
};

int main()
{
	//委托构造函数允许使用同一个类中的构造函数调用其他的构造函数
	//使用委托构造函数需要注意不能形成一个环(死循环）  要形成一条链  例如：我调用他，他调用它，它再调用她，
	//在初始化列表中调用了代理构造函数初始化类一个类成员变量后，就不能在初始化列表中再次初始化这个
	return 0;
}
