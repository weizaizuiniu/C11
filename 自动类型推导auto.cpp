#include <iostream>
using namespace std;
#include <string>
#include <map>


void test1()
{
	//使用auto声明的变量时，必须要初始化。编译器会在编译阶段根据初始化类型来推导出auto类型
	
	//auto还可以和指针、引用结合起来使用也可以带上const、volatile限定符，规则如下
	//当变量不是指针或者引用类型时，推导的结果不会保留const、volatile关键字
	//当变量是指针或者引用类型时，推导的结果会保留const、volatile关键字

	//没有const修饰
	//x:double
	auto x = 3.14;   
	 //y:int
	auto y = 520;   
	//z:char
	auto z = 'a';    
	//auto nb;         //error 变量必须初始化
	//auto double nb1; //error 语法错误

	int temp = 100;
	//&temp:int*
	//auto* a:int*
	auto* a = &temp;
	//&temp:int*
	//auto b:int*
	auto b = &temp;
	//temp:int
	//auto:int
	//auto& c:int&
	auto& c = temp;
	//d:int
	auto d = temp;

	//有const修饰的
	int tmp = 200;
	//a1:const int 
	const auto a1 = tmp;
	//a1:const int 
	//a2:const int 
	//a2:int
	auto a2 = a1;
	//tmp:int
	//a3:const int&
	const auto& a3 = tmp;
	//a3:const int&
	//a4:const int&
	auto& a4 = a3;
	//a1:const int
	//ptr4:const int*
	auto ptr4 = &a1;

}




class person
{
public:
    //auto v1 = 0;//error
    //static auto v2 = 0;//error 类的静态非常量成员不允许直接在类内初始化
	static const auto v3 = 10; //ok
};

//int fun(auto a, auto b)//error
//{
//    return a + b;
//}

template <typename T>
struct job {};

void test2()
{
	//auto的限制：
	//不能作为函数参数使用。因为只有在函数调用时才会给函数参数传递实参，auto要求必须要给修饰的函数赋值。
	//不能用于类的非静态成员变量的初始化 因为类的非静态成员变量是不属于类的 是对象的
	//不能使用auto关键字定义数组
	//无法使用auto推导出模板参数

	//cout << fun(10, 20) << endl; //error

	int arr[] = { 1,2,3,4,5 };
	auto a = arr;//ok  arr是数组地址 auto推导出int*
	//auto b[] = arr; //error  auto无法定义数组
	//auto c[] = { 1,2,3,4,5 };  //error  auto无法定义数组

	job<double>t;
	//iob<auto>t1 = t;//error

}

class T1
{
public:
	static int get()
	{
		return 10;
	}
};
class T2
{
public:
	static string get()
	{
		return "hello world";
	}
};
template <class T>
void func(void)
{
	auto ret = T::get();
	cout << ret << endl;
}
void test3()
{
	//用于STL容器的遍历
	//用于泛型编程  在使用模板的时候，很多情况下我们不知道应该定义什么类型。可以使用auto

	map<int, string> m;
	m.insert(make_pair(1, "tom"));
	m.insert(make_pair(2, "tiger"));
	m.insert(make_pair(3, "dog"));
	m.insert(make_pair(4, "cat"));

	//不使用auto的遍历：
	for (map<int, string>::iterator it = m.begin(); it != m.end(); it++)
	{
		//..........
	}

	//使用auto的遍历：
	for (auto it = m.begin(); it != m.end(); it++)
	{
		//..........
	}

	//用于泛型编程
	func<T1>();
	func<T2>();
}
int main()
{
	//test1();//auto自动类型推导规则

	//test2();//不推荐auto关键字使用的四个场景

	test3(); //推荐auto关键字使用的场景
	return 0;
}
