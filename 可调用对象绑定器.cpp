#include <iostream>
using namespace std;
#include <string>
#include <functional>//function包装器的头文件

//可调用对象绑定器
//std::bind用来将可调用对象与其参数一起绑定，绑定后的结构可以用std::function来保存，并延迟调用到任何我们需要的时候。
//两大作用：1、将可调用对象与其参数一起绑定成一个仿函数  2、将多元（参数个数>1）可调用对象转换成一元或者（n-1）元的可调用对象，即只绑定部分参数

//绑定非类成员函数/变量 语法：
//auto f = bind(可调用对象，绑定的参数/占位符);
//绑定类成员函数/变量 语法：
//auto f = bind(类函数/成员地址，类实例对象地址，绑定的参数/占位符);

//占位符：placeholders::_1 是一个占位符，表示这个位置将在函数调用时被传入第一个参数所代替。同时还有其他的占位符 placeholders::_2  laceholders::_3  placeholders::_4等.....

void output(int x, int y)
{
	cout << x << " " << y << endl;
}

void func(int x, int y, const function<void(int, int)>& f)
{
	if (x % 2 == 0)
	{
		f(x, y);
	}
}

void add(int x, int y)
{
	cout << "x: " << x << ", y: " << y << ", x+y: " << x + y << endl;
}

class Test
{
public:
	void print(int x, int y)
	{
		cout << x << " " << y << endl;
	}

	int m_number = 100;
};
int main()
{
	//测试：使用绑定器绑定可调用对象和参数，并调用得到的仿函数
	bind(output, 1, 2)();//输出：1 2
	bind(output, placeholders::_1, 2)(10);//输出: 10 2
	bind(output, 2, placeholders::_1)(10);//输出: 2 10

	//bind(output, placeholders::_2, 2)(10);//error  调用时没有第二个参数
	bind(output,2, placeholders::_2)(10,20);//调用时第一个参数10被吞掉了，没有被使用，所以输出：2 20

	bind(output, placeholders::_1,placeholders::_2)(10,20);//输出: 10 20
	bind(output, placeholders::_2,placeholders::_1)(10,20);//输出: 20 10

	//可调用对象绑定器的使用
	for (int i = 0; i < 10; ++i)
	{
		auto f = bind(add, i + 100, i + 200);
		//func(i, i, f);//因为bind的实参不是占位符，所以func的实参会被吞掉，如果想要func实参生效，可以使用占位符
	//输出结果 
	//x : 100, y : 200, x + y : 300
	//x : 102, y : 202, x + y : 304
	//x : 104, y : 204, x + y : 308
	//x : 106, y : 206, x + y : 312
	//x : 108, y : 208, x + y : 316
		auto f1 = bind(add, placeholders::_1, placeholders::_2);
		func(i, i, f1);
	//输出结果
	/*  x : 0, y : 0, x + y : 0
		x : 2, y : 2, x + y : 4
		x : 4, y : 4, x + y : 8
		x : 6, y : 6, x + y : 12
		x : 8, y : 8, x + y : 16*/
	}

	//成员函数绑定
	Test t;
	auto f2 = bind(&Test::print, &t, 520, placeholders::_1);
	f2(1314);//输出：520 1314
	
	//成员变量绑定
	auto f3 = bind(&Test::m_number, &t);
	cout << f3() << endl;//输出：100
	f3() = 666;
	cout << f3() << endl;//输出：666

	//包装器是无法对类函数/成员直接包装的，但是可以利用绑定器间接包装
	function<void(int,int)> f22 = bind(&Test::print, &t, 520, placeholders::_1);
	function<int&(void)>  f33 = bind(&Test::m_number, &t);//如果需要对f3()的值可读可写可以加引用


	return 0;
}
