#include <iostream>
using namespace std;
#include <map>
#include <string>

//利用typedef实现
template <typename T>
//typedef map<int, T>maptype;//typedef出错
struct mymap
{
	//解决办法 用结构体封装起来
	typedef map<int, T>maptype;
};

//利用using实现
template <typename T>
using mmap = map<int, T>;

void mytest(int a, string b)
{
	cout << a << "  " << b << endl;
}

//显然可以看出using在定义函数指针更加容易看懂
typedef void(*func)(int, string);
using func1 = void(*)(int, string);

template <typename T>
class person
{
public:
	void print(T& t)
	{
		auto it = t.begin();
		for (; it != t.end(); it++)
		{
			cout << it->first << ", " << it->second << endl;
		}
	}
};
int main()
{
	//在C++中可以通过typedef和using重定义一个类型
	typedef unsigned int u_int; //typedef 旧的类型  新的类型
	u_int a = 199;

	using lg =  long; //using 新的类型 = 旧的类型
	lg b = 23821;

	//通过两者的语法发现两者的使用没有太大的区别  定义函数指针的话 using的优势就显示出来
	func f = mytest;
	func1 f1 = mytest;
	f(10, "hello world");
	f1(20, "I love you");
	(*f)(10, "hello world");

	//有一种需求是typedef做不了的 就是给一个模板定义别名
	//要求map容器存储不同的对组：int-int  int-string  int-double
	map<int, string>m1;
	map<int, double>m2;
	map<int, int>m3;
	m1.insert(make_pair(10, "tom"));
	m2.insert(make_pair(20, 3.14));
	m3.insert(make_pair(30, 2));


	//这样太麻烦了 可以使用泛型编程思想 利用模板实现


	//mymap<string>::maptype mm1;  //利用typedef实现
	mmap<string> mm1;//利用using实现
	mm1.insert(make_pair(10, "tom"));
	mm1.insert(make_pair(20, "dog"));
	//person<mymap<string>::maptype> p1;
	person<mmap<string>> p1;
	p1.print(mm1);

	//mymap<int>::maptype mm2;  //利用typedef实现
	mmap<int> mm2;//利用using实现
	mm2.insert(make_pair(20,1));
	mm2.insert(make_pair(20,2));
	//person<mymap<int>::maptype> p2;
	person<mmap<int>> p2;
	p2.print(mm2);

	//mymap<double>::maptype mm3;  //利用typedef实现
	mmap<double> mm3;//利用using实现
	mm3.insert(make_pair(20,3.145));
	mm3.insert(make_pair(20,42.123));
	//person<mymap<double>::maptype> p3;
	person<mmap<double>> p3;
	p3.print(mm3);

	return 0;
}
