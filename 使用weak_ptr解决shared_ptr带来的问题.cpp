#include <iostream>
using namespace std;
#include <string>
#include <memory>//智能指针的头文件

//智能指针是存储指向动态分配（堆）对象指针的类，用于生存期的控制，能够确保在离开所在的作用域时，自动地销毁动态分配的对象，防止内存泄漏。
//智能指针的核心实现技术是引用计数，每使用一次，内部引用计数加一，每析构一次内部的引用计数减一，减为零时，删除所指向的堆内存。

//C++11中提供了三种智能指针，使用这些智能指针都需要包含头文件 <memory>

//shared_ptr:共享指针指针
//unique_ptr:独占的智能指针
//weak_ptr:弱引用的智能指针，它不共享指针，不能操作资源，是用来监视 shared_ptr 的

//shared_ptr 使用的注意事项：
/*1、不能使用一个原始地址初始化多个共享智能指针  （new出来的就是原始地址）
  2、函数不能返回管理了this的共享指针对象
  3、共享智能指针不能循环引用*/

struct Test:public enable_shared_from_this<Test>
{
	shared_ptr<Test> getShared_ptr()
	{
		//return shared_ptr<Test>(this);
		return shared_from_this();//第二个问题得以解决
	}
	~Test()
	{
		cout << "class Tset is disstruct ..." << endl;
	}
};

struct TA;
struct TB;

struct TA
{
	//shared_ptr<TB>bptr;
	weak_ptr<TB>bptr;//ok
	~TA()
	{
		cout << "class Tset is disstruct ..." << endl;
	}
};
struct TB
{
	shared_ptr<TA>aptr;
	~TB()
	{
		cout << "class Tset is disstruct ..." << endl;
	}
};
void testPtr()
{
	//3、共享智能指针不能循环引用,会导致它们的内存无法释放
	shared_ptr<TA>ap(new TA);
	shared_ptr<TB>bp(new TB);
	cout << "TA object use_count:" << ap.use_count() << endl;
	cout << "TB object use_count:" << bp.use_count() << endl;

	ap->bptr = bp;
	bp->aptr = ap;
	cout << "TA object use_count:" << ap.use_count() << endl;
	cout << "TB object use_count:" << bp.use_count() << endl;
	/*输出结果为：
		TA object use_count : 1
		TB object use_count : 1
		TA object use_count : 2
		TB object use_count : 2*/
	//说明没有销毁管理的那块内存

	//解决办法：weak_ptr
	/*输出结果：
		TA object use_count : 1
		TB object use_count : 1
		TA object use_count : 2
		TB object use_count : 1
		class Tset is disstruct ...
		class Tset is disstruct ...*/


}
int main()
{
	//弱引用智能指针是配合shared_ptr来使用的，
	//解决返回管理this的 shared_ptr ：如果shared_ptr包含了一个this指针，那么它会把释放两次
	//解决循环引用问题：
	//用一个共享智能指针来初始化一个弱引用智能指针，那么弱引用智能指针就可以监测着共享智能指针管理的那块内存

	//1、不能使用一个原始地址初始化多个共享智能指针
	Test* t = new Test;
	//shared_ptr<Test>ptr1(t);
	//shared_ptr<Test>ptr2(t);
	//上面的程序崩了，原因是Test对象被析构两次
	// 输出结果：
	//class Tset is disstruct ...
	//class Tset is disstruct ...

	shared_ptr<Test>ptr1(t);
	shared_ptr<Test>ptr2 = ptr1;//ok

	//2、函数不能返回管理了this的共享指针对象
	shared_ptr<Test>sp1(new Test);
	cout << "use_count:" << sp1.use_count() << endl;
	shared_ptr<Test>sp2 = sp1->getShared_ptr();//本质上是和第一个问题一样
	cout << "use_count:" << sp1.use_count() << endl;
	//上面的程序崩了，原因是Test对象被析构两次
	//输出结果：
    //use_count:1
	//use_count:1
	//class Tset is disstruct ...
	//class Tset is disstruct ...

	//这个问题可以通过weak_ptr来解决，返回管理this资源的共享智能指针对象shared_ptr。
	//C++11中提供了一个模板类叫做enable_shared_from_this<T>，这个类中有一个方法叫做shared_from_this(),
	//通过这个方法可以返回一个共享智能指针，在函数的内部就是使用weak_ptr来监测this对象，并通过调用weak_ptr的lock()方法返回一个shared_ptr对象。
	/*输出结果为：
		use_count : 1
		use_count : 2
		class Tset is disstruct ...
		class Tset is disstruct ...*/

	//3、共享智能指针不能循环引用
	testPtr();
	return 0;
}