#include <iostream>
using namespace std;
#include <string>
#include <list>
#include <map>

void test1()
{
	//decltype和auto不同的decltype是不需要进行初始化， 语法：decltype(表达式)
	int a = 10;
	//b->int
	decltype(a) b = 99;
	//c->double
	decltype(a + 3.14) c = 1.1111;
	//d->double
	decltype(a + b * c) d = 520.1314;

	//decltype可简单可复杂 这点是auto做不到的，auto只能推导已初始化的变量类型
}


class Test
{
public:
	int num = 9;
	string test;
	static const int value = 110;
};


void test2()
{
	//推导规则1：
    //表达式为普通变量或普通表达式或类表达式，在这种情况下，使用decltype推导出的类型和表达式的类型一致
	int x = 99;
	const int& y = x;
	//a:int
	decltype(x) a = x;
	//y:const int&
	//b:const int&
	decltype(y) b = x;
	//value:const int 
	//c:const int
	decltype(Test::value) c = 0;
	Test t;
	//t.test:string
	//d:string
	decltype(t.test) d = "hello world";
}


//函数声明 空实现
int func_int(){}
int& func_int_r() {}
int&& func_int_rr() {}
const int func_cint() {}
const int& func_cint_r() {}
const int&& func_cint_rr() {}
const Test func_cTest() {}

void test3()
{
	//推导规则2
	//表达式为函数调用时，使用decltype推导出的类型和函数返回值一致   注意：如果函数返回的是一个纯右值，需要忽略const和volatile限定符，只有类类型的不需要忽略
	int n = 100;
	//a:int
	decltype(func_int()) a = 0;
	//b:int&
	decltype(func_int_r()) b = n;
	//c:int&&
	decltype(func_int_rr()) c = 0;
	//d:int   这里返回纯右值，需要忽略const关键字
	decltype(func_cint()) d = 0;
	//e:const int&
	decltype(func_cint_r()) e = n;
	//f:const int&&
	decltype(func_cint_rr()) f = 0;
	//g:const Test
	decltype(func_cTest()) g = Test();

}


void test4() 
{
	//推导规则3：
	//表达式是一个左值，或者被括号（）包围，使用decltype推导出类型是表达式类型的引用（如果有const和volatile限定符不能忽略）

	const Test obj;
	//a:int  这里是一个普通的推导
	decltype(obj.num) a = 0;
	//b:const int&   这里是加了一个小括号的推导
	decltype((obj.num)) b = a;
    
	int n = 0, int m = 0;
	//c:int
	decltype(n + m) c = 0;
	//d:int&  因为表达式是一个左值
	decltype(n = n + m) d = n;
	//正如设想的一样 decltype不需要初始化
	decltype(m) xx;
}

template<class T>
class container
{
public:
	void print(T& c)
	{
		for (m_it = c.begin(); m_it != c.end(); m_it++)
		{
			cout << *m_it << endl;
		}
	}
private:
	//T::iterator m_it;  这里编译时发生错误 这个T编译器是不承认的 
	decltype(T().begin()) m_it;  //ok！
};
void test5()
{
	//关于decltype的应用很多出现在泛型编程中，比如我们编写一个类模板，在里边添加遍历容器的函数  目标：推导出迭代器类型
	list<int> ls{ 1,2,3,4,5,6,7,8,9 };
	const list<int> ls1{ 1,2,3,4,5,6,7,8,9 };
	list<int>::iterator it = ls.begin();//对刚刚的错误添加这一条语句
	container<list<int>> con;
	container<const list<int>> con1;
	con.print(ls);
	cout << endl;
	con1.print(ls1);
	
}
int main()
{
	//test1();//简单认识decltype

	//test2();//decltype的推导规则1

	//test3();//decltype的推导规则2

	//test4();//decltype的推导规则3

	test5();//decltype的应用场景
	return 0;
}
