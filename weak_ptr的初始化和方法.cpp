#include <iostream>
using namespace std;
#include <string>
#include <memory>//智能指针的头文件

//智能指针是存储指向动态分配（堆）对象指针的类，用于生存期的控制，能够确保在离开所在的作用域时，自动地销毁动态分配的对象，防止内存泄漏。
//智能指针的核心实现技术是引用计数，每使用一次，内部引用计数加一，每析构一次内部的引用计数减一，减为零时，删除所指向的堆内存。

//C++11中提供了三种智能指针，使用这些智能指针都需要包含头文件 <memory>

//shared_ptr:共享指针指针
//unique_ptr:独占的智能指针
//weak_ptr:弱引用的智能指针，它不共享指针，不能操作资源，是用来监视 shared_ptr 的


int main()
{
	//weak_ptr弱引用智能指针的初始化方式
	shared_ptr<int>sp(new int);

	weak_ptr<int>wp1;

	weak_ptr<int>wp2(wp1);

	weak_ptr<int>wp3(sp);

	weak_ptr<int>wp4;
	wp4 = sp;

	weak_ptr<int>wp5;
	wp5 = wp3;

	//weak_ptr
	//use_count()方法可以获取当前所观察的引用计数（不包含weak_ptr)
	cout << "use_count:" << endl;
	cout << "wp1:" << wp1.use_count() << endl;
	cout << "wp2:" << wp2.use_count() << endl;
	cout << "wp3:" << wp3.use_count() << endl;
	cout << "wp4:" << wp4.use_count() << endl;
	cout << "wp5:" << wp5.use_count() << endl;

	//expired() 判断资源是否被释放  还有共享智能指针管理返回false  否则返回true
	weak_ptr<int> weak(sp);
	cout << " 1.weak " << (weak.expired() ? "is" : "is not") << " expired" << endl;//1.weak is not expired 说明还有共享智能指针管理

	sp.reset();
	cout << " 2.weak " << (weak.expired() ? "is" : "is not") << " expired" << endl;//2.weak is not expired 说明没有共享智能指针管理

	//lock() 方法可以获取管理所检测资源的共享智能指针shared_ptr对象
	shared_ptr<int> sp1, sp2;
	weak_ptr<int>wp;

	sp1 = make_shared<int>(520);
	wp = sp1;
	sp2 = wp.lock();
	cout << "use_count: " << wp.use_count() << endl; //2

	sp1.reset();
	cout << "use_count: " << wp.use_count() << endl;// 1

	sp1 = wp.lock();
	cout << "use_count: " << wp.use_count() << endl;// 2

	cout << "sp1 = " << *sp1 << endl;// 520
	cout << "sp2 = " << *sp2 << endl;// 520

	//reset() 可以清空对象，使其不监测任何资源
	shared_ptr<int>sp3(new int(10));
	weak_ptr<int>wp6(sp3);
	cout << " 1.wp6 " << (wp6.expired() ? "is" : "is not") << " expired" << endl;//  1.wp6 is not expired

	wp6.reset();
	cout << " 2.wp6 " << (wp6.expired() ? "is" : "is not") << " expired" << endl;//  2.wp6 is expired 说明该对象没有监测任何资源


	return 0;
}