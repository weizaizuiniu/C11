#include <iostream>
using namespace std;

//右值引用是独立于值的，一个右值引用作为函数参数的形参时，在函数内部转发该参数给内部其他函数时，它会变成一个左值，并不是原来的类型了，
//如果需要按照参数原来的类型转发给另一个函数，可以使用 C++11 提供的 std::forward() 函数，该函数实现的功能称为完美转发。

//函数原型
//template <class T> T&& forward(typname remove_reference<T>::type& t) noexcept;
//template <class T> T&& forward(typname remove_reference<T>::type&& t) noexcept;

//当T为左值引用类型时，t将被转换为T类型的左值
//当T不是左值引用类型时，t将被转换为T类型的右值

template<typename T>
void printValue(T& t)//看起来是左值引用，说精细点是一个未定义引用类型，需要推导
{
	cout << "l-value" << t << endl;
}

template<typename T>
void printValue(T&& t)//看起来是右值引用，说精细点是一个未定义引用类型，需要推导
{
	cout << "r-value" << t << endl;
}

template<typename T>
void testForward(T&& v)
{
	printValue(v);
	printValue(move(v));
	printValue(forward<T>(v));
	cout << endl;
}

int main()
{
	testForward(520);
	//testForward 推导出是一个右值引用类型 在testForward内部的printValue(v)，右值引用传递就会变成左值引用  l - value520
	//printValue(move(v))把左值转换成一个右值  printValue传递过去的还是一个右值引用 r - value520
	//因为testForward的v是一个右值引用所以printValue(forward<T>(v))还是一个右值引用 r - value520
	int num = 100;
	testForward(num);
	testForward(forward<int>(num));
	//testForward(forward<int>(num))中，模板类型是int类型，它不是左值引用，所以forward将它转换成右值 其他的都和 testForward(520); 推导方式一样
	testForward(forward<int&>(num));
	testForward(forward<int&&>(num));

	//输出结果为：
	/*  l - value520
		r - value520
		r - value520

		l - value100
		r - value100
		l - value100

		l - value100
		r - value100
		r - value100

		l - value100
		r - value100
		l - value100

		l - value100
		r - value100
		r - value100*/


	return 0;
}