#include <iostream>
using namespace std;
#include <string>
#include <functional>

//Lambda表达式的一些优点:
//声明式的编程风格：就地匿名定义目标函数或函数对象，不需要额外写一个命名函数或函数对象。
//简洁：避免了代码膨胀和功能分散，让开发更加高效。
//在需要的时间和地点实现功能闭包，是程序变得更加灵活。

//Lambda表达式的语法
//[capture](params)opt->ret{body;};  
//其中capture是捕获列表
//params是参数列表，参数列表为空可以选择省略
//opt是函数选项，不需要可以省略。mutable：可以修改按值传递过来的拷贝（注意是能修改拷贝，不是值本身） exception：指定函数抛出的异常，如抛出整形异常，可以使用throw();
//ret是返回值类型，不需要可以省略。
//body是函数体。

//捕获列表的具体使用方式：
//[] - 不捕获任意变量
//[&]- 捕获外部作用域中所有变量，并作为引用在函数体内使用（按引用捕获）
//[=]- 捕获外部作用域中所有变量，并作为副本在函数体内使用（按值捕获）。   拷贝的副本在匿名函数体内部是只读的
//[=，%foo]- 按值捕获外部作用域中所有变量，并按照引用捕获外部变量foo
//[bar]- 按值捕获bar变量，同时不捕获其他变量
//[&bar]- 按值捕获bar变量，同时不捕获其他变量
//[this]- 捕获当前类的this指针。   让lambda表达式拥有和当前类成员函数同样的权限

//lambda表达式的类型在C++11中会被看做一个带有operator()的类，即仿函数  注意：没有捕获外部变量的lambda表达式会被看成普通函数
//按照C++的标准，lambda表达式的operator()默认是const的，一个const成员函数是无法改变成员函数值的

class Test
{
public:
	void output(int x, int y)
	{
		//auto x1 = [] {return m_number; };					 //error  没有捕获外部变量，不能使用类成员m_number
		auto x2 = [=] {return m_number + x + y; };			 //ok  以值拷贝的方法捕获所有外部变量
		auto x3 = [&] {return m_number + x + y; };			 //ok  以引用的方法捕获所有外部变量
		auto x4 = [this] {return m_number; };				 //ok  捕获this指针，可以访问对象内部成员
		//auto x5 = [this] {return m_number + x + y; };		 //error  只捕获this指针可以访问对象内部成员，但不能出现其他的外部变量
		auto x6 = [this, x, y] {return m_number + x + y; };  //ok  以值拷贝的方法捕获外部变量x,y 并捕获this指针
		auto x7 = [this] {return m_number++; };				 //ok  捕获this指针，可以修改对象内部成员
	}
	int m_number = 100;
};

void func(int x, int y)
{
	int a = 5;
	int b = 3;
	[=, &x](int z)mutable
	{
		int c = a;
		int d = x;
		b++;  //error 值拷贝的方法捕获变量是只读的 想要修改需要加上mutable关键字,但是修改的只是拷贝过来的值而不是修改真正的外部变量
		cout << b << endl;
	}(88);//这里没有小括号的话只是定义没有调用，加上（）就可以调用了 注意：lambda表达式有参数的话调用是需要指定对应的实参
	cout << b << endl;

	using ptr = void (*)(int);
	ptr p1 = [](int m)
	{
		cout << "m: " << m << endl;
	};
	p1(666);

	function<void(int)>f1 = [=](int m)
	{
		cout << "m: " << m << endl;
	};
	f1(777);

	function<void(int)>f2 = bind([=](int m)
	{
		cout << "m: " << m << endl;
	}, placeholders::_1);
	f2(888);
}
int main()
{
	func(100, 200);
	return 0;
}
